+++
title = "Dual-SIM Android Smartphones"
aliases = ["2011/01/dual-sim-android-smartphones.html"]
date = "2011-01-26T17:41:00Z"
[taxonomies]
tags = ["4.8\"", "Android", "Android 2.1 (Eclair)", "ARM", "Dion WO MID", "dual sim", "Marvell PXA935", "MIDs"]
categories = ["hardware"]
authors = ["peter"]
+++

When I first heard about Dual SIM smartphones, I almost couldn't believe that there were devices like this&mdash;back then (2008?) all Dual SIM phones were from rather unknown chinese manufacturers, often with KIRF looks and running an OS which you couldn't really call a smartphone OS&mdash;all smartphonish about these devices were their touchscreens.

<table cellpadding="0" cellspacing="0" class="tr-caption-container" style="float: right;margin-left: 1em;text-align: right"><tbody><tr><td style="text-align: center"><a href="dstl1_01m.jpg" style="clear: right;margin-bottom: 1em;margin-left: auto;margin-right: auto"><img src="dstl1_01m.jpg" /></a></td></tr><tr><td class="tr-caption" style="text-align: center">General Mobile DSTL1&mdash;image by General Mobile</td></tr></tbody></table>

As usability matters to me (and most of you, I assume) these devices really didn't seem to be a good choice, and so I rather went on carrying to phones (back then two EZX phones). Then Android came up, and I had high hopes for a smartphone with this operating system, be it just a basic AOSP version or the full experience with GMail, GTalk and Android Market.  

The first dual SIM smartphone that really catched attention was the General Mobile DSTL1 running Android 1.5 (Cupcake), which was announced at the MWC in 2009. Back then it had ok specs, 128MB Ram, a 624MHz Marvell PXA310 (ARMv5TE / XScale) SoC&mdash;it lacked 3G though, was EDGE only.

Since then we haven't seen much but announcements. Motorola (and others, like ZTE) have launched
some Dual-SIM phones in China, but since these are mostly (in fact all Motorola Dual-SIM phones I know of) CDMA+GSM, importing them to Europe doesn't help.

The most interesting device right now isn't quite a real smartphone, nor would one call it's internals high end by 2011 standards: It's the Shenzhen ACT Dion WO 4.8&#8221; MID phone <a href="http://twitter.com/charbax">charbax</a> (the man behind <a href="http://armdevices.net/">ARMDevices.net</a>) <a href="http://armdevices.net/2011/01/08/shenzhen-act-4-8-capacitive-marvel-pxa935-clamshell-android/">spotted at CES</a>. It had been first mentioned back in October at <a href="http://www.pocketables.net/2010/10/dion-computers-prototype-48-inch-clamshell-android-mid-revealed.html">pocketables.net</a>. To name the basic facts:
* Marvell PXA935 (&#8220;tri-core&#8221;/Sheeva, supports ARMv5TE, ARMv6 and ARMv7 architectures (so flash might be possible)) SoC, 
* 256MB of RAM, ROM/NAND/Flash memory unknown, microSD 
* DualSIM (once quad band EDGE + tri band WCDMA (3G) with HDSPA up to 3.6Mbps, once european (900, 1800, 1900) GSM with EDGE tri band (apparently the latter is achieved with a NXP5209 chip, no information about the other baseband chip (probably MTK?))
* WLan b/g, Bluetooth 2.1 EDR, GPS, chinese TV, FM Radio
* clamshell form-factor with QWERTY keyboard and optical mouse (just like the one on the Samsung SGH-i780)
* 4.8&#8221; WVGA capacitive touchscreen
* front facing 1.3M camera  (probably nice for video chat)
* 1920mAh battery for up to 4.5 hours talk time / up to ten days standby time

<table cellpadding="0" cellspacing="0" class="tr-caption-container" style="float: right;margin-left: 1em;text-align: right"><tbody><tr><td style="text-align: center"><a href="dion-mid.jpg" style="clear: right;margin-bottom: 1em;margin-left: auto;margin-right: auto"><img src="dion-mid.jpg" /></a></td></tr><tr><td class="tr-caption" style="text-align: center">Dion WO 4,8&#8221; MID (image from pocketables.net)</td></tr></tbody></table>

Of course this thing is huge (135 * 85 * 20mm, which almost similar to the 131.6 * 79 * 21.6mm HTC Universal (QTek9000/T-Mobile MDA Pro/&#8230;))  and Android 2.1 (Eclair) isn't too attractive nowadays (Gingerbread (2.3) brings Video chat, Froyo (2.2) is the minimum for Flash 10.1)&mdash;but if you've got large pockets, don't worry about no support and want Dual SIM really badly, this might be just the right device for you (bulk order price is said to be about 200$ each (but bulk order means large quantities).  (If you are really interested in this thing, check out <a href="http://forum.androidspin.com/showthread.php?4577-Storm1-%28Dual-SIM-MID%29/">this thread at androidspin.com</a>.)

### Conclusion:

With 512MB and a dedicated community that hacks the crap out of it, this could be a really great device&mdash;but being a pessimistic person I don't see that happening.

Despite this thing (which is, while not the best imaginable the best I could track down, (on the internet, not in real life)) you find some devices at eBay which are mostly MTK6516 based&mdash;which is a SoC that has a 460MHz ARM9 + a 280MHz ARM7 core&mdash;doesn't sound like high speed, does it? At least most of these things have a low WQVGA screen resolution (240x400 pixels) and 256MB Ram, so the user experience might be actually not totally ruined&mdash;but nontheless these are low end solutions which I will not recommend before I have had a satisfying hands on.

All in all, the Dual SIM market didn't improve much, even though there is such a powerful mobile OS like Android available for free&mdash;and to be honest, this is not too much of a surprise: Only small &#8220;outlaw&#8221; companies can afford making nice Dual SIM solutions&mdash;the big players prefer not to upset the huge network operators.
