+++
title = "Weekly GNU-like Mobile Linux Update (18/2023): Build your own phone, Phosh 0.27.0, and a Plasma Mobile progress report"
date = "2023-05-06T10:05:00Z"
draft = false
[taxonomies]
tags = ["Maui Project", "Sailfish OS", "Ubuntu Touch", "Plasma Mobile", "Phosh", "Genode", "Apache NuttX",]
categories = ["weekly update"]
authors = ["Peter",]
[extra]
author_extra = " with a submission by Don White and with friendly assistance from plata's awesome script"
+++

Also: Kaidan 0.9 finally brings OMEMO encryption, you can now Chat with GPT on Mobile Linux, a blog post on how to make Maui apps to look the way you want them to, a Sailfish Community News roundup, news in non-Linux (SMS on Apache NuttX on PinePhone and a new Genode release), sensor suppport for one Snapdragon 845 device and more! 

<!-- more -->

_Commentary in italics._

### Hardware
- [GitHub - evanman83/OURS-project: Step-by-step instructions to build a smartphone that is open-source, upgradeable, repairable, and Big Tech free.](https://github.com/evanman83/OURS-project/) via  [Hackaday](https://hackaday.com/2023/05/05/linux-cell-phone-build-ourphone/)

### Software progress
#### Gnome Ecosystem
- This Week in GNOME: [#94 Configuring Columns](https://thisweek.gnome.org/posts/2023/05/twig-94/). _Very nice app updates!_
- GNOME Shell & Mutter: [Vivid colors in Brno](https://blogs.gnome.org/shell-dev/2023/05/04/vivid-colors-in-brno/)
- Felipe Borges: [GNOME will be mentoring 9 new contributors in Google Summer of Code 2023](https://feborg.es/gnome-will-be-mentoring-9-new-contributors-in-google-summer-of-code-2023/)
- phosh.mobi: [Phosh 0.27.0](https://phosh.mobi/releases/rel-0.27.0/)
- Guido Günther: [phosh 0.27.0 is out 🚀📱:This one has a new menu on power button long press, initial emergency calls support (thanks @CoderThomasB, @devrtz),  improvements on the compositor side, new completers in phosh-osk-stub, configurable ring tones and more. Check out the full release notes at https://phosh.mobi/releases/rel-0.27.0/#phosh #librem5 @purism #gnome #linux #mobile #LinuxMobile #MobileLinux](https://social.librem.one/@agx/110299998553543353)

#### Plasma Ecosystem
- Plasma Mobile: [This Month in Plasma Mobile: April 2022](https://plasma-mobile.org/2023/05/05/this-month-plasma-mobile/) _Nice progress and some new apps!_
- Nate Graham: [Planning the future of Plasma](https://pointieststick.com/2023/05/05/planning-the-future-of-plasma/)
- Planet KDE: [Getting KDE Apps to our Users](https://jriddell.org/2023/05/05/getting-kde-apps-to-our-users/)
- Kaidan.im Blog: [Kaidan 0.9: End-to-End Encryption & XMPP Providers](https://kaidan.im/2023/05/05/kaidan-0.9.0/) _Finally, I'll definitely try this!_
- dot.kde.org: [Season of KDE 2023: Conclusion](https://dot.kde.org/2023/05/04/season-kde-2023-conclusion)
- eco.kde.org: [SoK 2023 Selenium-ST-API KDE Eco Power Measurement Proof Of Concept: Achieving Three KDE Goals With One Stone!](https://eco.kde.org/blog/2023-05-04-sok23-kde-eco-selenium-sum-up/)
- Kontact blog: [March/April in KDE PIM](https://kontact.kde.org/blog/2023/2023-05-03-kde-pim-march-april-2023/)
- Nicolas Fella: [Month three as KDE Software Platform Engineer](https://nicolasfella.de/posts/softwareplatform-month-3/)

#### Maui Project
- @mauiproject@floss.social: [Tweaking Maui Apps is super easy. The new blog entry covers all the ways to configure the Maui ecosystem:https://mauikit.org/blog/configuring-maui/#mauikit](https://floss.social/@mauiproject/110299245437385711)
- MauiKit blog: [Configuring Maui](https://mauikit.org/blog/configuring-maui/)

#### Sailfish OS
- [Sailfish Community News, 4th May, Docs Update](https://forum.sailfishos.org/t/sailfish-community-news-4th-may-docs-update/15565)
- adampigg on twitter: [latest kernel for the @pine64 #pinephonepro adds actual power management.  downside is that on resume the modem is in a weird state.  signal bar shows no service and cant make calls ... however, incoming calls work once the modem has re-registered.  some investigation to do!](https://twitter.com/adampigg/status/1652950352440840193#m)

#### Ubuntu Touch
- fredldotme on twitter: [Thanks to the great UBports community I'm able to get a OnePlus Pad for development purposes and bringing #UbuntuTouch to it!](https://twitter.com/fredldotme/status/1654163454482079744#m)
- fredldotme on twitter: [The O.G. Xperia X Ubuntu Touch port is back on my radar. Right now I'm in the process of bringing it up to parity with other focal devices, improving stability and... *drumroll*... FINALLY fixing the green camera we all were annoyed by!](https://twitter.com/fredldotme/status/1653627564131246082#m)

#### Distributions
- [Adrian Campos Garrido on Twitter: "Just updated SDM845 to Kernel 6.2.0 in @openSUSE images for mobiles like Oneplus 6/6T, Pocophone F1, Shiftmq6 and other with this chipset model https://t.co/NjRpENpafm"](https://twitter.com/hadrianweb/status/1653428888268165121)

#### Apps
- LinuxPhoneApps.org: Apps: [Bavarder](https://linuxphoneapps.org/apps/io.github.bavarder.bavarder/)
- [Matt Lewis: "I released version 0.2.7 of Pygenda - an agenda app targeting keyboard-equipped mobile devices. The UI is inspired by Agenda apps on Psion PDAs. ... It's alpha software and it would be great if people could test it on similar devices (a Pine Phone + keyboard should be ideal). Main requirements are Python3.5+ & GTK3"](https://h4.io/@semiprime/110318428548187797)

#### Stack
- Phoronix: [Mesa 23.1 Inches Closer To Release With RC4 Released](https://www.phoronix.com/news/Mesa-23.1-RC4)

#### Non-Linux
- Genode: [Sculpt OS release 23.04](https://genode.org/news/sculpt-os-release-23.04) _I somehow missed this last week._
- Lup Yuen: [NuttX RTOS for PinePhone: Phone Calls and Text Messages](https://lupyuen.github.io/articles/lte2)
  - PinePhoneOfficial (Reddit): [NuttX RTOS for PinePhone: Phone Calls and Text Messages](https://www.reddit.com/r/PinePhoneOfficial/comments/136aodh/nuttx_rtos_for_pinephone_phone_calls_and_text/)

#### Matrix
- Matrix.org: [This Week in Matrix 2023-05-05](https://matrix.org/blog/2023/05/05/this-week-in-matrix-2023-05-05)

#### Stack
- Phoronix: [Mesa Vulkan KHR_present_wait Support Extended To Wayland](https://www.phoronix.com/news/Mesa-KHR_present_wait-Wayland)

### Worth Noting
- r/postmarketOS: [What is missing from Linux mobile today?](https://www.reddit.com/r/postmarketOS/comments/1372p81/what_is_missing_from_linux_mobile_today_discussion/)
- r/postmarketOS: [Daily Driver Status Update April-May 2023](https://www.reddit.com/r/postmarketOS/comments/134p8wx/daily_driver_status_update_aprilmay_2023/)
- Mobian Wiki: [location - added python script to convert log file into gpx file](https://wiki.mobian-project.org/doku.php?id=location&rev=1683070735&do=diff)
- Purism community: [Script: Put Fotos into Year and Month Subfolder](https://forums.puri.sm/t/script-put-fotos-into-year-and-month-subfolder/20192)
- @calebccff@fosstodon.org: [it's finally here!!https://gitlab.com/postmarketOS/pmaports/-/merge_requests/4050](https://fosstodon.org/@calebccff/110294952037124276) _Sensor support for the Shift6mq, yay!_
- Phones (Librem 5),- Purism community: [New Post: Introducing Flatpaks on PureOS](https://forums.puri.sm/t/new-post-introducing-flatpaks-on-pureos/20173)

### Worth Reading
- LaoBlog: [Pinephone review: Arch](https://blog.libero.it/Laoblog2/16612378.html)
- LINux on MOBile: [Having a stand at Linux-Infotag 2023](https://linmob.net/lit-2023-stand-experiences/)

### Worth Watching
- NOT A FBI Honey pot: [Discord from the terminal on your PinePhone #shorts #linux](https://www.youtube.com/watch?v=mcEwbhK3x0U)
- AndroidHowTo: [Ubuntu Touch Version 10 - Stable / Linux for Poco X3 NFC Update: 230417](https://www.youtube.com/watch?v=yCihLUHLJSg)
- AndroidHowTo: [How to Install Waydroid and WhatsApp in UBUNTU Touch - Android in Linux](https://www.youtube.com/watch?v=2Swuq4_9C0A)
- Sailfish official: [Xperia 10 mark 3 install sailfish os 4.5.0.19](https://www.youtube.com/watch?v=32UM-5N3p1g)
- RTP Tech Tips: [Pinetab2 Potential / Pinetab RISC-V Looking Back At Original Pinetab (Warning: Expect Bugs [early]) - YouTube](https://www.youtube.com/watch?v=-8g1UD-DceY). _Missed this, thank you, Don, for telling me about it!_

### Thanks
Huge thanks to Plata for [the nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speed up collecting links from feeds by a lot.

### Please note
This one is a bit early due to taking a week of from work and travels to Lake Como. This update was written on bad mobile internet in rural germany, and then bad train internet in Switzerland - I hope it's still 'good enough'. Due to being abroad and taking a break, help with next weeks weekly update (again to be written on a lengthy train ride on May 13th), is even more appreciated than usual.

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A?edit) for the next one! Since I am collecting many things there, this get's you early access if you will ;-) __If you just stumble on a thing, please put it in there too - all help is appreciated!__

