+++
title = "Weekly GNU-like Mobile Linux Update (13/2023): Sxmo 1.14 + updates by Nemo Mobile and Plasma Mobile"
draft = false
date = "2023-04-02T21:25:00Z"
[taxonomies]
tags = ["Ubuntu Touch","Sailfish OS","Plasma Mobile","Nemo Mobile",]
categories = ["weekly update"]
authors = ["peter"]
[extra]
author_extra = "supported by Padraic, ptrshb, KDG (+ friendly assistance from plata's awesome script)"
+++

Also: A last hurray for Ubuntu Touch 16.04 with OTA 25, Qt 5.15 and Kirigami on Sailfish OS, and more.
<!-- more -->
_Commentary in italics._

### Software progress

#### Ubuntu Touch
- UBports News: [Ubuntu Touch OTA-25 Release](http://ubports.com/blog/ubports-news-1/post/ubuntu-touch-ota-25-release-3890) _I've booted my Nexus 4 for a last update - and it's great!_
  - [UBports: "Please note that all Xenial Devel and RC builds will be halted over the next few days.…"](https://mastodon.social/@ubports/110117725238182302)
- UBports News: [PinePhone and PinePhone Pro](http://ubports.com/blog/ubports-news-1/post/pinephone-and-pinephone-pro-3889)

#### GNOME ecosystem
- This Week in GNOME: [#89 Steady Framerates](https://thisweek.gnome.org/posts/2023/03/twig-89/)
- GNOME Shell & Mutter: [Ensuring steady frame rates with GPU-intensive clients](https://blogs.gnome.org/shell-dev/2023/03/30/ensuring-steady-frame-rates-with-gpu-intensive-clients/)


#### Plasma ecosystem	
- Nate Graham: [This week in KDE: it’s the little things that count](https://pointieststick.com/2023/03/31/this-week-in-kde-its-the-little-things-that-count/)
- Plasma Mobile: [This Month in Plasma Mobile: March 2023](https://plasma-mobile.org/2023/03/29/this-month-plasma-mobile/)
- Volker Krause: [February/March in KDE Itinerary](https://www.volkerkrause.eu/2023/03/31/kde-itinerary-february-march-2023.html)
- ~redstrate: [My work in KDE for March 2023](https://redstrate.com/blog/2023/03/my-work-in-kde-for-march-2023/)

#### Nemo Mobile
- Nemo Mobile UX team: [Nemomobile in March 2023](https://nemomobile.net/pages/nemomobile-in-march-2023/)

#### Sailfish OS
- Sailfish OS Forum, piggz: [Developer Announcement - Qt 5.15 available for app developer testing](https://forum.sailfishos.org/t/developer-announcement-qt-5-15-available-for-app-developer-testing/15105)
- [Adam Pigg (@adampigg): "#KDE #Kirigam2 coming to a #SailfishOS device near you. @JollaHQ @kdecommunity"](https://nitter.privacytools.io/adampigg/status/1641519114387705856#m)

#### Sxmo
- Sxmo: [Sxmo 1.14.0 released — sourcehut lists](https://lists.sr.ht/~mil/sxmo-announce/%3CCRJLVDQWO5TS.3BLF8Y8EG3GGG%40yellow-orcess%3E)

#### Linux
- Phoronix: [Panfrost Driver For Linux 6.4 Adds Speed Binning, New MediaTek SoCs](https://www.phoronix.com/news/Panfrost-Linux-6.4-New-Hardware)
- [[PATCH v4 0/6] dts: qcom: arm64: sdm845: SLPI DSP enablement - Dylan Van Assche](https://lore.kernel.org/phone-devel/20230401173523.15244-1-me@dylanvanassche.be/)

#### Non-Linux
- [GitHub - lupyuen/pinephone-nuttx-usb: PinePhone USB Driver for Apache NuttX RTOS](https://github.com/lupyuen/pinephone-nuttx-usb#ls-crashes-when-usb-hub-support-is-enabled)

#### Matrix
- Matrix.org: [This Week in Matrix 2023-03-31](https://matrix.org/blog/2023/03/31/this-week-in-matrix-2023-03-31)
- Matrix.org: [Security releases: matrix-js-sdk 24.0.0 and matrix-react-sdk 3.69.0](https://matrix.org/blog/2023/03/28/security-releases-matrix-js-sdk-24-0-0-and-matrix-react-sdk-3-69-0)

### Worth noting
- Purism community: [More Librem 5 apps! – A tutorial to compile Flutter apps for Librem 5 on x64 machine](https://forums.puri.sm/t/more-librem-5-apps-a-tutorial-to-compile-flutter-apps-for-librem-5-on-x64-machine/19844) _This is cool!_
- [Widevine support (Netflix,Spotify, Disney+...) for aarch64-devices like the Pinephone could be coming soon](https://github.com/raspberrypi/Raspberry-Pi-OS-64bit/issues/248)
- [Adam: "#pinephone signal app flatpak …"](https://fosstodon.org/@elagost/110123411837625820) _Please get involved!_
- [Robert Mader: "Pretty cool milestone for cameras on the #PinePhonePro and the #libcamera + #PipeWire stack today: the #Manjaro images (the stock distro) for both #phosh and #PlasmaMobile now come with all required packages by default.…"](https://floss.social/@rmader/110118286604103829)
  - [Robert Mader: "@dubstar\_04 I personally don't…"](https://floss.social/@rmader/110130531672001382) _Plans for libcamera 0.0.5._
- [TuxPhones: "It's the Yearly Poll™ time again! Do you use any of the following as a daily driver? …" - Fosstodon](https://fosstodon.org/@tuxdevices/110113774114147715) _Go vote and share your experiences!_
- [Diamond: "Discord on a Nokia 2780! I wrote it using a small version of my Go Discord library. It's a PWA app that also works on desktops.…"](https://hachyderm.io/@diamond/110110488677695118)

### Worth reading
- PINE64: [March Update: Tablet Bonanza!](https://www.pine64.org/2023/04/01/march-update-tablet-bonanza/)
  - PINE64official (Reddit): [March Update: Tablet Bonanza! | PINE64](https://www.reddit.com/r/PINE64official/comments/128t203/march_update_tablet_bonanza_pine64/)
- Make Use Of: [8 Things to Do After Unboxing a Purism Librem 5](https://www.makeuseof.com/things-to-do-after-unboxing-purism-librem-5/) _Better use apt full-upgrade!_
- OMG!Linux: [Tuba is a Magnificent New Mastodon App for Linux](https://www.omglinux.com/tuba-gtk-mastodon-client-for-linux/) _Tuba is great, but I'm pretty sure Joey never tried Tootle 2.0 alpha..._

### Worth listening
- Jupiter Broadcasting: [Linux Action News 286](https://www.youtube.com/watch?v=CswKJbfG718) _Brief bit on Ubuntu Touch!_

### Worth watching
- PINE64: [March Update: Tablet Bonanza!](https://www.youtube.com/watch?v=SujNfkegedM), [Odysee](https://odysee.com/@PINE64:a/march-update-tablet-bonanza!:1)
- FSFE: [Flashing OnePlus 6T with postmarketOS](https://media.fsfe.org/w/iqFYfsVy8reYHLhz3Ftmgq)
- The Linux Experiment: [Ubuntu 23.04 beta, Linux Phone improvements, Italy blocks chatGPT: Linux & Open Source News](https://www.youtube.com/watch?v=i9fhkpGdv0Y)
- CodeInc: [Ubuntu Touch OTA Release Based on Ubuntu 20.04 LTS](https://www.youtube.com/watch?v=s39skT5KeUc)
- LiKoToN: [ChatGPT on Sailfish OS](https://www.youtube.com/watch?v=qIjaqJlZV-Y)
- Tech Box: [This is NOT READY - Linux on Tablets](https://www.youtube.com/watch?v=C9_S_q8A8N0)
- Ubuntu OnAir: [Ubuntu Desktop Team Indaba - March 31, 2023](https://www.youtube.com/watch?v=pES9LfKCttA) _This episode will be all about UBports and the amazing strides they’ve made over the years. Alfred has been around in the UBports space for a number of years and since it uses Mir, we have a resident Mir expert, Alan. So please come and learn about UBports and Mir, and ask any questions you have!_
- Canal do Lukita: [WhatsApp on Ubuntu Touch #linuxphone](https://www.youtube.com/watch?v=YYy5iO7P6_o)
- Canal do Lukita: [Banco Itaú on Ubuntu Touch #linuxphone #bancoitau #itausa #ubuntu](https://www.youtube.com/watch?v=griLMI29CYM)
- Canal do Lukita: [Trying install Nubank on Ubuntu Touch #linuxphone #linux #ubuntu #nubank](https://www.youtube.com/watch?v=oe9IBHAdjeY)
- Canal do Lukita: [Playing Super Mario on Ubuntu Touch (GBC Emulator) #linuxphone #supermario #ubuntu #nintendo](https://www.youtube.com/watch?v=yzq_6oL1blU)

### Thanks
Huge thanks to adraic, ptrshb, KDG and again to Plata for [the nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speeds up collecting links from feeds by a lot.

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A?edit) for the next one! Since I am collecting many things there, this get's you early access if you will ;-) __If you just stumble on a thing, please put it in there too - all help is appreciated!__


