+++
title = "Weekly GNU-like Mobile Linux Update (15/2023): postmarketOS 22.12 SP2 and a Maui Report"
draft = false
date = "2023-04-16T21:00:00Z"
[taxonomies]
tags = ["Phosh","GNOME Mobile","PineTab V","PineTab2","postmarketOS","Ubuntu Touch","Maui","Unified Push","LinuxPhoneApps",]
categories = ["weekly update"]
authors = ["peter"]
[extra]
author_extra = " (with friendly assistance from plata's awesome script)"
+++

Also: Two blog PinePhone related blog posts by Megi, progress on Pipewire for call audio and on libcamera-powered video recording and more!.
<!-- more -->
_Commentary in italics._

### Software progress

#### GNOME ecosystem
- This Week in GNOME: [#91 Inverted Titles](https://thisweek.gnome.org/posts/2023/04/twig-91/)
  - Regarding Wike being adaptive, [see this issue](https://github.com/hugolabe/Wike/issues/122#issuecomment-1509708679)

#### Plasma ecosystem	
- Nate Graham: [This week in KDE: “Make multi GPU not suck”](https://pointieststick.com/2023/04/14/this-week-in-kde-make-multi-gpu-not-suck/)
- Maui blog: [Maui Report 22](https://mauikit.org/blog/maui-report-22/)
- Volker Krause: [Deploying UnifiedPush on Linux](https://www.volkerkrause.eu/2023/04/15/kde-unifiedpush-on-linux.html)
- Carl Schwan: [Kirigami Addons 0.8.0](https://carlschwan.eu/2023/04/13/kirigami-addons-0.8.0/)
- Carl Schwan: [Announcing Arianna 1.0](https://carlschwan.eu/2023/04/13/announcing-arianna-1.0/)
- Albert Vaca: [The road to KDE Connect 2.0](https://albertvaka.wordpress.com/2023/04/11/the-road-to-kde-connect-2-0/)
- KDE Eco: [Season Of KDE 2023 With KDE Eco: Improvements To KdeEcoTest](https://eco.kde.org/blog/2023-04-14-sok23-eco-tester/)

#### Ubuntu Touch
- [UBports: "#Xenial 16.04 is to get a #hotfix…"](https://mastodon.social/@ubports/110195992277709736) 

#### Distributions
- postmarketOS Blog: [v22.12 SP2: The One With The Nice Pull-Down Menu](https://postmarketos.org/blog/2023/04/10/v22.12.2-release/)
  - Lemmy - postmarketOS: [v22.12 SP2: The One With The Nice Pull-Down Menu](https://lemmy.ml/post/945757)

#### Linux
- [[PATCH v3 0/5] Add support for Focaltech FTS Touchscreen - Joel Selvaraj](https://lore.kernel.org/phone-devel/20230415020222.216232-1-joelselvaraj.oss@gmail.com/#r)

#### Non-Linux
- [NuttX RTOS for PinePhone: 4G LTE Modem](https://lupyuen.github.io/articles/lte)

#### Matrix
- Matrix.org: [This Week in Matrix 2023-04-14](https://matrix.org/blog/2023/04/14/this-week-in-matrix-2023-04-14)
- Matrix.org: [This Week in Matrix 2023-04-10](https://matrix.org/blog/2023/04/10/this-week-in-matrix-2023-04-10)

#### Stack
- Phoronix: [Mesa 23.1-rc1 Published For Testing With Many Graphics Driver Updates](https://www.phoronix.com/news/Mesa-23.1-rc1)

### Worth noting
- u/PossiblyLinux127: [How is the security for linux phones?](https://www.reddit.com/r/PINE64official/comments/12hlnlg/how_is_the_security_for_linux_phones/)
- MobileLinux: [Mobile GNOME development brings pin unlock screen](https://www.reddit.com/r/mobilelinux/comments/12hca74/mobile_gnome_development_brings_pin_unlock_screen/)
* [Sebastian Krzyszkowiak: "Librem 5 waking up from system suspend on incoming calls…"](https://social.librem.one/@dos/110180625035442292)
* [PipeWire Project: "PipeWire 0.3.69 is out now with fixes…"](https://fosstodon.org/@pipewire/110190587218720021)
* [Guido Günther: "Completely forget: I've tagged version 0.0.3 of μPlayer…"](https://social.librem.one/@agx/110184844885409115)
* [Cédric Bellegarde: "I was missing this since I moved from Phosh to GNOME Shell…"](https://floss.social/@gnumdk/110188159924542483)
* [Robert Mader: "@adamplumb and everyone interested in #PipeWire + #GStreamer video recording:…"](https://floss.social/@rmader/110177325684910054)
* [caleb ×: "op6(t) and SHIFT6mq users on postmarketOS edge…"](https://fosstodon.org/@calebccff/110208551946197695)
* [Adrien Plazas: "I'm looking for someone to take my Metronome app over. It's tagetting @gnome and it's based on Rust and Libadwaita…"](https://mamot.fr/@KekunPlazas/110184774250739575)

### Events

While a dedicated blog post will follow in the coming week, if you're in southern Germany on April 29th, 2023, you may enjoy [this stand](https://www.luga.de/static/LIT-2023/stands/#linux-auf-mobilger%C3%A4ten)!

### Worth reading
- megi's PinePhone Development Log: [Pinephone DRM driver issues](https://xnux.eu/log/#080). _[DRM: Direct Rendering Manager](https://en.wikipedia.org/wiki/Direct_Rendering_Manager)._
- megi's PinePhone Development Log: [Pinephone Backlight Testing](https://xnux.eu/log/#079)
- PINE64: [PineTab-V and PineTab2 launch](https://www.pine64.org/2023/04/10/pinetab-v-and-pinetab2-launch/)
  - PINE64official (Reddit): [PineTab-V and PineTab2 launch | PINE64](https://www.reddit.com/r/PINE64official/comments/12hqf3n/pinetabv_and_pinetab2_launch_pine64/)
- Hugo Barrera: [Installing postmarketOS on a OnePlus 6 with an encrypted filesystem](https://whynothugo.nl/journal/2023/04/12/installing-postmarketos-on-a-oneplus-6-with-an-encrypted-filesystem/)
- LinuxPhoneApps.org: [New apps of LinuxPhoneApps.org, Q1/2023: Easier Editing Edition](https://linuxphoneapps.org/blog/new-listed-apps-q1-2023-easier-editing/)
- Phoronix: [Lenovo Yoga Laptops Getting Tablet Mode Switch Driver With Linux 6.4](https://www.phoronix.com/news/Lenovo-Yoga-Tablet-Mode-Switch)
- Drew DeVaults blog: [The Free Software Foundation is dying](https://drewdevault.com/2023/04/11/2023-04-11-The-FSF-is-dying.html)

### Maybe Worth watching
- mikehenson5: [PinePhone Pro - Flash DanctNIX Arch with Phosh](https://www.youtube.com/watch?v=FBGVUq3DGK4)
- mikehenson5: [PinePhone OR PinePhone Pro - Phosh - Setup - Updates - Packages](https://www.youtube.com/watch?v=-nbUXABJw0c)
- BG2050: [Porting Ubuntu Touch to my A71.](https://www.youtube.com/watch?v=B8r3u_SQma4). _Lengthy live stream._
- Niko: [PostmarketOS edge with Software Center & bootsplash, Gnome Shell Mobile w/ Gnome 44, PIN lock screen](https://www.youtube.com/watch?v=_btJ2g8ba14)
- Jhony Kernel: [Install android di nokia N900 (dualboot) maemo/nitdroid](https://www.youtube.com/watch?v=si1yY021COg)
- Marchscarf: [Silly motivational bit, but it shows that I have fun in spite of my progress ~ pphone OG Maemo Leste](https://www.youtube.com/watch?v=xX4PatlUMME)
- artur silver: [Xiaomi Redmi Note 7 Droidian, com servidores, Nextcloud+OnlyOffice+Mail server+Wordpress](https://www.youtube.com/watch?v=OjEFuirDRp0)
- Niccolò Ve: [KDE Apps are GROWING!](https://tube.kockatoo.org/w/ef8e0bc8-ef84-47c6-a0e5-c8bb806c596f)
- Twinntech: [I put a SIM card in my Pinephone!](https://www.youtube.com/watch?v=DSnAeHcXIv8)

### Thanks
Huge thanks to adraic, ptrshb, KDG and again to Plata for [the nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speeds up collecting links from feeds by a lot.

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A?edit) for the next one! Since I am collecting many things there, this get's you early access if you will ;-) __If you just stumble on a thing, please put it in there too - all help is appreciated!__


