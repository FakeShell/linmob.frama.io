+++
title = "Weekly GNU-like Mobile Linux Update (10/2023): Double Digits already"
draft = false
date = "2023-03-12T22:37:00Z"
[taxonomies]
tags = ["Sailfish OS", "Mobian","Librem 5","Flathub"]
categories = ["weekly update"]
authors = ["peter"]
[extra]
author_extra = " (with friendly assistance from plata's awesome script)"
+++

Three Mobian blog posts, a Sailfish Community News Round Up, and another write-up on cameras by Martijn Braam!
<!-- more -->
_Commentary in italics._

### Software progress

#### GNOME ecosystem
- This Week in GNOME: [#86 New Decoding](https://thisweek.gnome.org/posts/2023/03/twig-86/)
- Marcus Lundblad: [Maps and GNOME 44](http://ml4711.blogspot.com/2023/03/maps-and-gnome-44.html)
- [Phosh 0.25.2 · Phosh](https://phosh.mobi/releases/rel-0.25.2/)

#### Plasma ecosystem	
- Nate Graham: [This week in KDE: Qt apps survive the Wayland compositor crashing](https://pointieststick.com/2023/03/10/this-week-in-kde-qt-apps-survive-the-wayland-compositor-crashing/)
- KDE Announcements: [KDE Ships Frameworks 5.104.0](https://kde.org/announcements/frameworks/5/5.104.0/)
- Nicolas Fella: [How platform integration in Qt/KDE apps works](https://nicolasfella.de/posts/how-platform-integration-works/)
- KDAB: [Qt 6 Debugging in Visual Studio and VS Code](https://www.kdab.com/debug-qt-in-vs-and-vsc/)
- Volker Krause: [January/February in KDE PIM](https://www.volkerkrause.eu/2023/03/08/kde-pim-january-february-2023.html)
- Nicolas Fella: [A month as KDE Software Platform Engineer](https://nicolasfella.de/posts/softwareplatform-month-1/)
- Rishi Kumar: [Season of KDE Blog #1](https://k3yss.github.io/posts/sok_blog1/). _This is work on an offline mode for Tokodon!_

#### Sailfish OS
- [Sailfish Community News, 9th March, Help Articles](https://forum.sailfishos.org/t/sailfish-community-news-9th-march-help-articles/14965)
- [Yann Büchau: "Well, that was surprisingly easy: @kde's dolphin file manager running in an @alpinelinux #Apptainer on :sailfishos: #SailfishOS. Okular works fine as well! Full touch, scroll, pinch-to-zoom support etc. Quite cool! No menu bars though, no rotation and popups are weird, but hey!…"](https://fosstodon.org/@nobodyinperson/110006306105964335), [How to](https://fosstodon.org/@nobodyinperson/110007970761465975)

#### Distributions
- Mobian Blog: [The non-free-firmware repository](https://blog.mobian.org/posts/2023/03/11/non-free-firmware/)
- Mobian Blog: [These months in Mobian: February/March 2023](https://blog.mobian.org/posts/2023/03/10/tmim/)
- Mobian Blog: [Porting Mobian to New Devices](https://blog.mobian.org/posts/2023/03/07/porting-to-new-devices/)
- Breaking updates in pmOS edge: [Changes due to the release of mkinitfs 2.0](https://postmarketos.org/edge/2023/03/09/mkinitfs-2.0/)

#### Hardware enablement
- [Subject: [PATCH 00/10] Add RT5033 charger device driver](https://lore.kernel.org/lkml/cover.1677620677.git.jahau@rocketmail.com/)
  Ongoing work on mainlining a missing charging driver for RT5033 used in a few phones with MSM8916, ie. Samsungs A5 2015, S4 Mini VE, Grand Max. Unsure if newsworthy before it's mainlined, but interesting to keep an eye on it. _Thank you, anonymous contributor!_

#### Non-Linux
- Lup Yuen: [(Clickable) Call Graph for Apache NuttX Real-Time Operating System](https://lupyuen.github.io/articles/unicorn2)

#### Stack
- Phoronix: [Wayland Clients Can Now Survive Qt Wayland Crashes / Compositor Restarts](https://www.phoronix.com/news/Qt-Wayland-Compositor-Restart)
- Phoronix: [PipeWire 0.3.67 Fixes Stuttering For Some Bluetooth Devices](https://www.phoronix.com/news/PipeWire-0.3.67-Released)
- Phoronix: [Mesa 22.3.7 Released To End Out The Series](https://www.phoronix.com/news/Mesa-22.3.7-Released)

#### Matrix
- Matrix.org: [This Week in Matrix 2023-03-10](https://matrix.org/blog/2023/03/10/this-week-in-matrix-2023-03-10)

### Worth noting
- JR-Fi on Purism Community: [Using a SONY QX10 (and QX100) lenscamera with L5 possible!](https://forums.puri.sm/t/using-a-sony-qx10-and-qx100-lenscamera-with-l5-possible/19660). _This is pretty cool - maybe especially considering other Linux Phones that don't have camera support yet!_
- Captain_Morgan on Purism community: [Testing Quectel EM12-G WWAN 4G Modem on Librem 5](https://forums.puri.sm/t/testing-quectel-em12-g-wwan-4g-modem-on-librem-5/19683)

### Worth reading
- Robert McQueen: [Flathub in 2023](https://ramcq.net/2023/03/07/flathub-in-2023/). _Promising!_
- Martijn Braam: [Mobile Linux camera pt6](https://blog.brixit.nl/mobile-linux-camera-pt-6/)
- Purism: [Where is My Librem 5? Part 3](https://puri.sm/posts/where-is-my-librem-5-part-3/)
- Purism: [Update for the Librem 5 Support in Mainline Linux](https://puri.sm/posts/update-for-the-librem-5-support-in-mainline-linux/)
- Nick Desaulniers: [Disambiguating Arm, Arm ARM, Armv9, ARM9, ARM64, Aarch64, A64, A78, ...](https://nickdesaulniers.github.io/blog/2023/03/10/disambiguating-arm/)

### Worth watching
- Low Orbit Flux: [Pine Phone - 2023 - postmarketOS with Phosh](https://www.youtube.com/watch?v=XhF1t-6ZSOA)
- willis8: [Librem 5 video!](https://www.youtube.com/watch?v=POys9giVsK0). _I suppose that this was shot on a Librem 5._
- Techlore: [Quad9 vs Sony, & The State of Linux Phones! - Techlore Talks 7](https://www.youtube.com/watch?v=k7tFnSeqnJY). _Oof._
- Digital Wandering: [Waydroid and Ubuntu Touch - with Improved Video support - on my PIXEL 3A](https://www.youtube.com/watch?v=aanxQC8slBw) 
- totodesbois100: [Test Cell Broadcast on Ubuntu Touch with ofono-phonesim modem emulator](https://www.youtube.com/watch?v=ALyiEn-XQ3E)
- Continuum Gaming: [Microsoft Continuum Gaming E355: SFOS with AD – Do the most popular Android apps work with MicroG?](https://www.youtube.com/watch?v=RJ0YO2MIfyY)
- GNUtoo: [How Replicant, a 100% free software Android distribution, uses (or doesn't use) Guix
](https://fosdem.org/2023/schedule/event/replicantguix/). _I somehow missed this one._

### Thanks
Huge thanks again to Plata for [the nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speeds up collecting links from feeds by a lot.

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A?edit) for the next one! Since I am collecting many things there, this get's you early access if you will ;-) __If you just stumble on a thing, please put it in there too - all help is appreciated!__

