+++
title = "HTC Universal on Debian with X"
aliases = ["2008/04/30/htc-universal-on-debian-with-x.html"]
date = "2008-04-30T17:32:00Z"
[taxonomies]
tags = ["debian lenny", "development", "HTC Universal", "software", "Titchy Mobile"]
categories = ["impressions", "software"]
authors = ["peter"]
+++
My Universal is on <a href="http://www.debian.org/releases/testing">Debian Lenny</a> / <a href="http://wiki.neilandtheresa.co.uk/Titchy_Mobile">Titchy Mobile</a> again and it is working great (of course), even X is working.
Debian is a nice distribution, and I really like it, since I first tried it. But there was one thing I did not like since that first time: relatively old packages&mdash;I prefer to be relatively near to bleeding edge, if there is a new release, I want to try it out soon.
<!-- more -->
<a href="http://gpe.linuxtogo.org/">GPE</a> in Debian is still 2.6; which means it is quite old (2.7 was released in early 2006, unstable packages aren't newer), and that makes it much less interesting.

<center>
    <img style="margin: 10px;" src="titchymobile-screenshot_1_gpe.png" alt="Screenshot of Titchy Mobile: running GPE" alt="Screenshot of Titchy Mobile running GPE" title="Screenshot of Titchy Mobile running GPE" />
    <img style="margin: 10px;" src="titchymobile-screenshot_2_hildon.png" alt="Screnshot Titchy Mobile running Hildon" title="Screnshot Titchy Mobile running Hildon" />
</center>


And it looks weird as you can see on the picture, matchbox-desktop seems to be taken from <a href="http://www.pokylinux.org/">poky</a>&mdash;because poky's sago looks much like that. As I prefer to have some folders in the background, I am now running <a href="http://www.icewm.org/">icewm</a> which is nice and fast.

BTW: I tried to install hildon (<a href="http://www.maemo.org/">Maemo's</a>' UI), but apparently, there are not all packages&mdash;the interface looked horrible (lower image).

Another problem in Debian are browsers with a small footprint&mdash;I could not find a <a href="http://software.twotoasts.de/?page=midori">midori</a> package yet, nor a <a href="http://gpe.linuxtogo.org/projects/gpe-mini-browser.shtml">gpe-mini-browser package</a>. I chose <a href="http://www.gnome.org/projects/epiphany/">epiphany-webkit</a>, as I like webkit a lot, but it is to thick to be usable&mdash;that's why I use <a href="http://www.dillo.org/">dillo</a> now, which runs fast, though it doesn't render todays websites well. But it isn't much worse than WM6s' IE and pretty fast&mdash;not bad.

The same issue with special or adjusted apps, like <a href="http://www.google.de/url?sa=t&amp;ct=res&amp;cd=1&amp;url=http%3A%2F%2Fwww.abisource.com%2F&amp;ei=lLIYSP7sC6C-mwO0vpjzCw&amp;usg=AFQjCNHjdXKD-M-BOhdyF2ChAcOen8aZaQ&amp;sig2=bALHKFp9Jpe0HF-yZqsLOA">abiword</a> <a href="http://www.qoheleth.uklinux.net/blog/?p=137">for embedded devices</a>, with another UI, no printing support but many import filters to edit or at least view documents on the go. Nothing of this in Debian&mdash;but hey, it can't contain everything.

I think I'll have to compile all this stuff myself, which will be a nice experience, but I think it is worth doing it as vpnc works and I can use HTC Universal as a micro notebook (smaller than EEE, UMTS, Touchscreen, longer lasting battery ;) ).

And to sum it all up: New kernel would be nice, improvements on power management (show batterys' power) and wifi are very welcome, maybe a easy to setup easy to use graphical phone app (I think about using zenity, xdialog or something like that to work with htcunid like whiptail does).
